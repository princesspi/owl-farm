#! /bin/sh
find /usr/share/nginx/rduke_com/owlfarm/uploads/ -mmin +1440 -exec shred -u -n 100 {} \;
find /usr/share/nginx/private_rduke_com/files/ -mmin +1440 -exec shred -u -n 100 {} \;
