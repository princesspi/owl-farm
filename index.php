<!DOCTYPE html>
<html>
<head>
<title>Owl Farm</title>
<?php require('includes/scripts.html'); ?>
<link rel="stylesheet" href="style.css">
<?php require("includes/meta.html"); ?>
</head>
<body>
<!-- Header -->
<h1 class="flat">Owl Farm Encrypted File Sharing</h1>
<br>
<?php include("includes/menu.html"); ?>

<!-- Encrypted -->
<h3>Encrypted File Sharing</h3>
<div class="uploadbox">
	<input type="file" id="file"><br>
	<span id="loading">Loading, please wait...</span><br>
	<p id="success"></p>
</div>

<!-- Unencrypted (uguu) -->
<h3>Unencrypted File Sharing</h3>
<p>Based on <a href="https://github.com/nokonoko/Uguu">uguu</a> - <a href="owlfarm.json">ShareX Config File</a></p>
<div class="uploadbox">
	<form action="api.php?d=upload" method="post" id="uform" enctype="multipart/form-data">
		<input type="hidden" name="MAX_FILE_SIZE" value="150000000" />
		<input type="file" id="unencrypted" name="file" />
		<input type="hidden" id="randomname" name="randomname" value="checked"/>
		<!--<button  type="submit">Upload</button>-->
	</form>
</div>


<div>
<h3>Paste Text</h3>
<p><a href="owlfarm.json">ShareX Config File</a></p>
<form action="paste.php" method="post">
<textarea name="paste" rows="20" cols="40"></textarea><br>
<input type="submit" value="Submit">
</form>
</div>
<script src="scripts/index.js"></script>
</body>
</html>
