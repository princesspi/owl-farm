<!DOCTYPE html>
<html>
<head>
<title>Free / Anonymous Services - Owl Farm</title>
<link rel="stylesheet" href="style.css">
<?php require("includes/meta.html"); ?>
</head>
<body>
<h1>Free and Anonymous Services</h1>
<h2>File Hosts</h2>
  <ul>
    <li><a href="https://yiff.moe">Yiff.moe</a> - 512mb</li>
    <li><a href="https://desu.sh/index.html">Desu.sh</a> - 512mb</li>
    <li><a href="https://mixtape.moe/">Mixtape.moe</a> - 100mb</li>
    <li><a href="https://mixtape.moe/">Mixtape.moe</a> - 100mb</li>
    <li><a href="https://nya.is/">Nya.is</a> - 100mb</li>
    <li><a href="http://anon-f.com/">Anon-f</a> - 100mb - 30 days - <b>NO HTTPS</b></li>
    <li><a href="https://pomf.cat/">Pomf.cat</a> - 75mb</li>
    <li><a href="https://share.riseup.net">Share by Riseup</a> - 50mb - 7 days</li>
    <li><a href="https://cocaine.ninja/">Cocaine.ninja</a> - 32mb</li>
  </ul>
<h2>Image Hosts</h2>
  <ul>
    <li><a href="https://img42.com/">Img42</a> - 10m/30m/forever</li>
  </ul>
<?php include("includes/menu.html"); ?>
</body>
</html>