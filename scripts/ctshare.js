// document.getElementById helper
function $id(id) {
	return document.getElementById(id);
}

function stringify(raw) {
	return CryptoJS.enc.Latin1.stringify(raw);
}

function pdkdf2(passphrase) {

    var salt = CryptoJS.SHA3(passphrase, { outputLength: 256 });
    var key = CryptoJS.PBKDF2(passphrase, salt, { keySize: 256/32, iterations: 1024  });
	
	return stringify(key);
}


function encrypt(message,passphrase) {
	return CryptoJS.AES.encrypt(message, pdkdf2(passphrase));
}

function decrypt(ciphertext,passphrase) {
	return stringify(CryptoJS.AES.decrypt(ciphertext, pdkdf2(passphrase)));
}

//function success(data) { alert(data); }