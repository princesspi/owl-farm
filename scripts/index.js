function loadFile() {
	var passphrase = prompt("Passphrase Please (16 chars min)");
	if(passphrase.length <16) { 
		alert("Password must be at least 16 characters long!");
		return false;
	}
	var file = $id("file").files[0];
	var reader = new FileReader();
	
	reader.onloadend = function () {
			var ciphertext = btoa(encrypt(reader.result,passphrase));
			$.post("upload.php",{file:ciphertext},function(data){$id("success").innerHTML=data;});
		}
	
		if (file) {
			reader.readAsDataURL(file);
		} 
}

$id("file").onchange = function() {
	$id("loading").style.display = "inline";
	loadFile();
}

$id("unencrypted").onchange = function() {
	$id("uform").submit();
}