<?php if(preg_match("/^[a-f0-9]{40}$/",$_GET['file'])) { $file = $_GET['file']; } ?>
<!DOCTYPE html>
<html>
<head>
<title>Owl Farm - View File</title>
<?php require('includes/scripts.html'); ?>
<script src="scripts/view.php?file=<?=$file?>"></script>
<link rel="stylesheet" href="style.css">
<?php require("includes/meta.html"); ?>
</head>
<body>
<?php include("includes/menu.html"); ?>
<h3>Please allow popup windows on this page.</h3>
<p>Please be patient, this process may take up to a minute (average is 10 seconds) and the browser may become unresponsive.</p>
</body>
</html>