<?php
$min = $_GET['min'];
$max = $_GET['max'];
$len = $_GET['len'];

if($min < 0) { echo 'ERROR: Minimum is 0'; die();}
if($max > 256) { echo 'ERROR: Maximum is 256'; die();}
if($len > 10000) { echo 'ERROR: Maximum generated values is 10,000'; die();}

$enough = true;
$counter = 0;

while($enough)
{
	$bytes = openssl_random_pseudo_bytes(1,$secure);
	$hex = bin2hex($bytes);
	$dec = hexdec($hex);
	if($dec >= $min && $dec <= $max)
	{
		if($counter == $len) { $enough = false; }
		$out[$counter] = $dec;
		$counter++;
	}
}

echo 'var ctrand = ' . json_encode($out) . ';';
?>
