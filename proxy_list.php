<!DOCTYPE html>
<html>
<head>
<title>Proxy Subscription - Owl Farm</title>
<link rel="stylesheet" href="style.css">
<?php require("includes/meta.html"); ?>
</head>
<body>
<h1>Owl Farm Proxy List / Free Proxy Subscription</h1>
<?php include("includes/menu.html"); ?>
<p>A list of proxies compatable with the most proxy subscription services like with <a href="https://getfoxyproxy.org/">FoxyProxy</a></p>
<p>All proxies are tailored to your needs and delivered fresh on every reload for maximum reliability</p>
<p>Tests proxies from ~50 sources via an implementation of <b><a href="http://proxybroker.readthedocs.io/en/latest/">ProxyBroker</a></b></p>
<form action="relay.php" method="POST">
<b>Proxy Type</b><br>
  <input type="checkbox" name="types[]" value="HTTP" id="HTTP"> <label for="HTTP">HTTP</label> (Unencrypted)<br>
  <input type="checkbox" name="types[]" value="HTTPS" id="HTTPS"> <label for="HTTPS">HTTPS</label><br>
  <input type="checkbox" name="types[]" value="SOCKS4" id="SOCKS4"> <label for="SOCKS4">SOCKS4</label><br>
  <input type="checkbox" name="types[]" value="SOCKS5" id="SOCKS5" checked="yes"> <label for="SOCKS5">SOCKS5</label><br><br>
<b>Anonymity Level</b><br>
  <input type="checkbox" name="lvl[]" value="Transparent" id="Transparent"> <label for="Transparent">Transparent</label> (Leaks your IP)<br>
  <input type="checkbox" name="lvl[]" value="Anonymous" id="Anonymous"> <label for="Anonymous">Anonymous</label> (Reveals you are using a proxy)<br>
  <input type="checkbox" name="lvl[]" value="High" id="High" checked="yes"> <label for="High">High</label> (Reveals nothing)<br><br>
<b><input id="obs" type="checkbox" name="obs" value="0"> <label for="obs">Obsfuscate With Base64 (Prevents Bot Harvesting)</label></b> <br><br>
<b><input type="text" name="nation" id="nation" value="US"> <label for="nation">Country (2 digit code, multiple seperated by spaces)</label></b>
<b>Number of Proxies Per Load (1-50)</b> <input type="text" size="2" name="limit" value="5"><br><br>
    <input type="submit" value="Generate Proxy Subscription">
</form>
</body>
</html>